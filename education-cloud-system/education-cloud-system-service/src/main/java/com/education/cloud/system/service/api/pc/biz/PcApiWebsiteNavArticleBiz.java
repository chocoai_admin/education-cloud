package com.education.cloud.system.service.api.pc.biz;

import com.education.cloud.system.common.req.WebsiteNavArticleSaveREQ;
import com.education.cloud.system.common.req.WebsiteNavArticleUpdateREQ;
import com.education.cloud.system.common.req.WebsiteNavArticleViewREQ;
import com.education.cloud.system.common.resq.WebsiteNavArticleViewRESQ;
import com.education.cloud.system.service.dao.WebsiteNavArticleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.education.cloud.system.service.dao.impl.mapper.entity.WebsiteNavArticle;
import com.education.cloud.util.base.Result;
import com.education.cloud.util.enums.ResultEnum;
import com.education.cloud.util.tools.BeanUtil;

/**
 * 站点导航文章
 *
 */
@Component
public class PcApiWebsiteNavArticleBiz {

	@Autowired
	private WebsiteNavArticleDao dao;

	public Result<WebsiteNavArticleViewRESQ> view(WebsiteNavArticleViewREQ req) {
		if (req.getNavId() == null) {
			return Result.error("NavId不能为空");
		}
		WebsiteNavArticle record = dao.getByNavId(req.getNavId());
		return Result.success(BeanUtil.copyProperties(record, WebsiteNavArticleViewRESQ.class));
	}

	public Result<Integer> save(WebsiteNavArticleSaveREQ req) {
		if (req.getNavId() == null) {
			return Result.error("NavId不能为空");
		}
		WebsiteNavArticle record = BeanUtil.copyProperties(req, WebsiteNavArticle.class);
		int results = dao.save(record);
		if (results > 0) {
			return Result.success(results);
		}
		return Result.error(ResultEnum.SYSTEM_SAVE_FAIL);
	}

	public Result<Integer> update(WebsiteNavArticleUpdateREQ req) {
		if (req.getId() == null) {
			return Result.error("Id不能为空");
		}
		WebsiteNavArticle record = BeanUtil.copyProperties(req, WebsiteNavArticle.class);
		int results = dao.updateById(record);
		if (results > 0) {
			return Result.success(results);
		}
		return Result.error(ResultEnum.SYSTEM_SAVE_FAIL);
	}

}
