package com.education.cloud.course.feign.interfaces;

import com.education.cloud.course.feign.qo.CourseRecommendQO;
import com.education.cloud.course.feign.vo.CourseRecommendVO;
import com.education.cloud.util.base.Page;
import com.education.cloud.util.constant.ServiceConstant;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 课程推荐
 *
 * @author wujing
 */
@FeignClient(name = ServiceConstant.COURSE_SERVICE,contextId = "courseRecommendClient")
public interface IFeignCourseRecommend {

    @RequestMapping(value = "/feign/course/courseRecommend/listForPage", method = RequestMethod.POST)
    Page<CourseRecommendVO> listForPage(@RequestBody CourseRecommendQO qo);

    @RequestMapping(value = "/feign/course/courseRecommend/save", method = RequestMethod.POST)
    int save(@RequestBody CourseRecommendQO qo);

    @RequestMapping(value = "/feign/course/courseRecommend/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/feign/course/courseRecommend/update", method = RequestMethod.PUT)
    int updateById(@RequestBody CourseRecommendQO qo);

    @RequestMapping(value = "/feign/course/courseRecommend/get/{id}", method = RequestMethod.GET)
    CourseRecommendVO getById(@PathVariable(value = "id") Long id);

}
