package com.education.cloud.course.feign.interfaces;

import com.education.cloud.course.feign.qo.DicQO;
import com.education.cloud.course.feign.vo.DicVO;
import com.education.cloud.util.base.Page;
import com.education.cloud.util.constant.ServiceConstant;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 数据字典
 *
 * @author wujing
 */
@FeignClient(name = ServiceConstant.COURSE_SERVICE,contextId = "dicClient")
public interface IFeignDic {

    @RequestMapping(value = "/feign/course/dic/listForPage", method = RequestMethod.POST)
    Page<DicVO> listForPage(@RequestBody DicQO qo);

    @RequestMapping(value = "/feign/course/dic/save", method = RequestMethod.POST)
    int save(@RequestBody DicQO qo);

    @RequestMapping(value = "/feign/course/dic/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/feign/course/dic/update", method = RequestMethod.PUT)
    int updateById(@RequestBody DicQO qo);

    @RequestMapping(value = "/feign/course/dic/get/{id}", method = RequestMethod.GET)
    DicVO getById(@PathVariable(value = "id") Long id);

}
