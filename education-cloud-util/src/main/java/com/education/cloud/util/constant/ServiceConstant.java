package com.education.cloud.util.constant;

/**
 * @Description 服务名常量
 * @Date 2020/3/21
 * @Created by 67068
 */
public interface ServiceConstant {

    /**
     * 用户服务名称
     */
    String USER_SERVICE = "education-cloud-user-service";

    /**
     * 课程服务名称
     */
    String COURSE_SERVICE = "education-cloud-course-service";

    /**
     * 系统管理服务名称
     */
    String SYSTEM_SERVICE = "education-cloud-system-service";
}
